# A New Digital Manifesto

My current statement of beliefs about our inalienable digital rights on the modern Internet.

## Can I link to it, quote it, or reference it?

Yes.

While content changes are unlikely at this point, you can also fork or link to
an [archived version](https://web.archive.org/web/*/anewdigitalmanifesto.com) if
you're concerned about post-link changes.

## Can I fork it or distribute it?

Yes. The site is designed to be easy to fork and re-host.

- It can be hosted statically with no serverside code.
- Image links are relative, so the site can be hosted as a subdirectory instead
  of as a top-level domain (ie, Github/Gitlab pages).
- There are no Javascript requirements, and the site should degrade gracefully
  on even older browsers.
  
## What's the build process?

There isn't one. Pages are hand-maintained HTML. Clone or download the repo, and
you're good to go. No extra steps.

For simplicity's sake, the build process is unlikely to change in the future
unless something happens to make manual maintenance across too many files
cumbersome.

## Can I get the logos?

Yes.

The logos are available in a raw, uncompressed format as Krita files in the
source directory.

## Can I contribute, fix errors, or build on this?

Yes. Public Domain content and an Open codebase mean that you do not need to ask
my permission to build on top this site.

However, if you do want my permission or you want your stuff merged into the
main site, you can reach out via the issue tracker, via a pull request, or via
email at contact@anewdigitalmanifesto.com.

If you want to make a Git patch but are uncomfortable using Gitlab for any
reason, I also accept patches over email. If you have a suggestion but are not
technically inclined, email is the best way to contact me.

## Can I translate it?

Please do!

The following translations are currently available:

- [English](https://anewdigitalmanifesto.com)
- [Spanish](https://anewdigitalmanifesto.com/spanish)

## Why isn't X included?

Probably because:

- I don't think it's a right, OR
- I do think it's a right, but it's not a _digital_ right, OR
- I do think it's a digital right, but it's not an inalienable digital right, OR
- I do think it's an inalienable digital right, but it's already summed up by
  one or more of the other rights.
  
The goal of A New Digital Manifesto isn't to go into massive amounts of detail
on every digital right you have; it's to sum up our fundamental, shared digital
rights in an accessible format, and to lay a common groundwork people can build
on.

Some details are purposefully ambiguous, because (within reason) some
flexibility and diversity among interpretations is encouraged.

## Authors and Contributors:

- [danShumway](https://danshumway.com)
- [Paula](https://terceranexus6.gitlab.io/terce/)
