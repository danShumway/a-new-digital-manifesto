<!DOCTYPE html>
<html lang="en-US">
    <!-- Copyright 2019, Daniel Shumway

         Permission is hereby granted, free of charge, to any person obtaining a
         copy of this software and associated documentation files (the "Software"),
         to deal in the Software without restriction, including without
         limitation the rights to use, copy, modify, merge, publish, distribute,
         sublicense, and/or sell copies of the Software, and to permit persons
         to whom the Software is furnished to do so, subject to the following
         conditions:

         The above copyright notice and this permission notice shall be included
         in all copies or substantial portions of the Software.

         THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
         OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
         MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
         IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
         CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
         TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
         SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>A New Digital Manifesto</title>
        <meta name="description"
              content="Six inalienable digital rights for the modern age.">

        <meta property="og:title"
              content="A New Digital Manifesto">
        <meta property="og:description"
              content="Six inalienable digital rights for the modern age.">
        <meta property="og:locale"
              content="en_US">
        <!-- Additional translations welcome, get in touch if you would like to make one. -->

        <!-- TODO sometime, come up with a favicon -->
        <!-- <link rel="icon" type="image/png" href="/"> -->
        <meta property="og:image"
              content="img/og-image.png">

        <meta property="twitter:card"
              content="summary">
        <meta property="twitter:title"
              content="A New Digital Manifesto">
        <meta property="twitter:description"
              content="Six inalienable digital rights for the modern digital age.">
        <meta property="twitter:image"
              content="img/twitter-image.png">
        <meta property="twitter:image:alt"
              content="The six logos corresponding to each digital right">

        <!-- Don't overcomplicate things. Inlining forces us to be conservative with included CSS. -->
        <style>

         /* Resets */
         a { color: black; text-decoration: none; }
         a:visited { color: black; text-decoration: none; }
         html, body { margin: 0; background-color: rgb(250, 247, 247); }
         body { margin: 1em; }

         /* Basic styling */
         main { margin-left: auto;
             margin-right: auto;
             max-width: 35em; }

         /* Font list shamelessly copied from Simplenote. If it ain't broke, don't fix it.
          * That being said, I may try to revisit this later to improve consistency. */
         section { margin-top: 5em;
             font-size: 1.2em;
             font-family: 'Noto Serif', serif; }

         header, h1, h2 {
             font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif; }

         /* Title Tweaks */
         header { margin-top: 7em;
             margin-bottom: 10em; }

         h1 { font-size: 5em;
             margin-bottom: 0; }

         h2 { text-align: center; }

         section img { display: block;
             margin-right: auto;
             margin-left: auto;
             max-width: 10em;
             width: 100%; }

         header p { font-size: 1.4em;
             margin: 0.5em 0; }

         /* Copyright */
         footer { width: 100%;
             max-width: 60em;
             margin-top: 8em;
             margin-left: auto;
             margin-right: auto; }

         footer a { color: blue; }

         .license { float: left;
             margin-right: 1em;
             padding-top: 0.2em; }

        </style>
    </head>

    <body>
        <main>
            <article>
                <header>
                    <h1>A New Digital Manifesto</h1>
                    <p>Our inalienable rights within the digital age</p>
                </header>

                <section id="right-to-communicate">
                    <img alt="Right to Community Logo" src="img/communicate.png" />
                    <h2><a href="#right-to-communicate">
                        The Right to Communicate
                    </a></h2>

                    <p>Users and communities have the right to communicate with
                    one another, both publicly and privately. Users have the
                    right to encrypt or hide communications that they wish to be
                    private. No one has the right to preemptively force a user
                    to reveal or broadcast who they are communicating with, or
                    even that they are communicating at all.</p>

                    <p>Users have the right to publicly publish information and
                    content, even if it is not directed at a specific person or
                    recipient. By extension, users also have the right to access
                    publicly published information and content.</p>

                    <p>Users have the right to build systems that will allow
                    them to communicate, publicly or privately. Users have the
                    right to teach other people how to communicate, and to
                    distribute software that will aid in communication.</p>
                </section>

                <section id="right-to-filter">
                    <img alt="Right to Filter Logo" src="img/filter.png" />
                    <h2><a href="#right-to-filter">
                        The Right to Filter
                    </a></h2>

                    <p>Both users and communities have the right to filter and
                    organize the content they consume and host, and to block
                    communications that they do not wish to receive. This can be
                    done either manually or via automated means. No one has the
                    right to force a user to consume content without their
                    permission.</p>

                    <p>Users and communities have the right to share filters,
                    whether those filters take the form of software, algorithms,
                    or manually curated blocklists and allowlists.</p>

                    <p>Users have the right to subvert software and platform
                    restrictions that would force them to consume content that
                    they do not wish to see, and to teach other people how to
                    subvert these restrictions. Users have the right to
                    distribute software that circumvents technological
                    restrictions for the purposes of personal content filtering.</p>
                </section>

                <section id="right-to-remember">
                    <img alt="Right to Remember Logo" src="img/remember.png" />
                    <h2><a href="#right-to-remember">
                        The Right to Remember
                    </a></h2>

                    <p>Users have the right to archive and index information
                    online. Users have the right to share information with
                    others, to share indexes of that information with others,
                    and to tell other people that the information exists.</p>

                    <p>Users have the right to break or circumvent geoblocks,
                    DRM, and other technological mechanisms that would prevent
                    them from discovering, viewing, and archiving information
                    that they have lawful access to.</p>
                </section>

                <section id="right-to-hide">
                    <img alt="Right to Hide Logo" src="img/hide.png" />
                    <h2><a href="#right-to-hide">
                        The Right to Hide
                    </a></h2>

                    <p>Users have the right to take measures that hide their
                    identity online and in real-life. Users have the right to
                    form multiple identities, and to choose which identities
                    they want to use to communicate, transact, publish, or
                    consume content. Users have the right to simultaneously
                    exercise their Right to Hide and their other digital rights.</p>

                    <p>When people or platforms attempt to obtain personal
                    information from a user or to forcibly associate them with a
                    single identity, users have the right to lie and to
                    subvert technologies that would unmask them.</p>

                    <p>Users have the right to build and distribute software
                    that hides their identity. Users have the right to teach
                    other people how to subvert software and how to lie to
                    organizations and individuals that attempt to deanonymize
                    them.</p>
                </section>

                <section id="right-to-delegate">
                    <img alt="Right to Delegate Logo" src="img/delegate.png" />
                    <h2><a href="#right-to-delegate">
                        The Right to Delegate
                    </a></h2>

                    <p>Users have the right to authorize other users, software,
                    and organizations to perform legal actions online on their
                    behalf. No one has the right to compel a user to only
                    exercise their rights in person, or through manual
                    processes.</p>

                    <p>Users have the right to build and distribute software
                    that automates a legal action they could take. Users have
                    the right to circumvent and subvert restrictions that would
                    block automated or third-party agents from performing a
                    legal action or accessing legal content on their behalf.</p>

                    <p>Users have the right to teach other people how to
                    circumvent restrictions on delegation and automation. Users
                    have the right to distribute software that aids in
                    circumventing these restrictions.</p>
                </section>

                <section id="right-to-modify">
                    <img alt="Right to Modify Logo" src="img/modify.png" />
                    <h2><a href="#right-to-modify">
                        The Right to Modify
                    </a></h2>

                    <p>Users have the right to inspect and modify code and
                    content that is placed on a device or inserted into an
                    environment that they own. Users may exercise this right
                    regardless of whether or not they own the code/content. No
                    one has the right to control what a user does with
                    code/content on their own device.</p>

                    <p>In addition to inspecting and modifying code, users have
                    the right to tell other people about their discoveries.
                    Users have the right to teach other people how to perform
                    similar inspections and modifications. Users also have the
                    right to distribute software that assists other people in
                    inspecting and modifying local code and content.</p>
                </section>
            </article>
        </main>

        <footer>
            <a rel="license" class="license"
               href="http://creativecommons.org/publicdomain/zero/1.0/">
                <img src="/img/cc0-button.png" style="border-style: none;" alt="CC0" />
            </a>

            <p>Released as Public Domain, maintained by Daniel Shumway.<br>
            For questions, comments, and criticism, write to
                <a href="mailto:contact@anewdigitalmanifesto.com">
                    contact@anewdigitalmanifesto.com</a>.</p>
        </footer>
    </body>

</html>

